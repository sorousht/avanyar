[![Netlify Status](https://api.netlify.com/api/v1/badges/4fecd90b-2f61-4c81-9bfa-38b7ff0f3984/deploy-status)](https://app.netlify.com/sites/hardcore-lamarr-b71522/deploys)

# Smart Team Web
A website for a smart team!


## Running
```bash
# development
yarn start

# production build
yarn build
```