import React from "react";
import { graphql } from "gatsby";
import { ServiceTile, ContactForm, StepTile, Layout, Navbar } from "../components";
import { css } from 'glamor';

const heroBody = (data) => css({
  background: `url(${data.cover.childImageSharp.fixed.src}) no-repeat center center`,
  backgroundSize: "cover",
});

const heroText = css({
  color: "white",
  "text-shadow": `0 0 4px #1f3c88`
});

const IndexPage = ({ data }) => (
  <Layout
    logo={data.logo}
    seo={data.seo}
    map={data.map}
    hasNavbar={false}
  >
    <section id="hero" className="hero is-primary is-large">
      <div className="hero-head">
        <Navbar
          logo={data.logo}
          seo={data.seo}
          isPrimary={true}
        />
      </div>
      <div className="hero-body" {...heroBody(data)}>
        <div className="container has-text-centered">
          <h2 className="title is-2 is-spaced" {...heroText}>ارائه راه‌کارهای امنیتی و شبکه</h2>
          <h2 className="title is-4 is-spaced" {...heroText}>برای خانه، محل کار و سازمان شما</h2>
        </div>
      </div>
    </section>
    <section className="hero is-medium">
      <div id="services" className="hero-body">
        <div className="container has-text-centered">
          <h1 className="title">
            خدمات
            </h1>
          <h2 className="subtitle">
            رضایت، کمترین زمان و ضمانت در ارائه خدمات باعث اعتماد مشتریان به ما شده‌است
            </h2>
          <ServiceTile services={data.services} />
        </div>
      </div>
    </section>
    <section id="workflow" className="hero is-medium is-light">
      <div className="hero-body">
        <div className="container is-fluid has-text-centered">
          <h1 className="title">
            روش‌کار
            </h1>
          <h2 className="subtitle">
            مراحلی که از درخواست شما تا تحویل کار طی می‌شود
            </h2>
          <StepTile steps={data.steps} />
        </div>
      </div>
    </section>
    <section id="contact" className="hero is-medium">
      <div className="hero-body">
        <div className="container">
          <h1 className="title has-text-centered">
            راه‌های تماس با ما
            </h1>
          <h2 className="subtitle has-text-centered">
            برای ما بنویسید، در کوتاه‌ترین زمان ممکن با شما تماس خواهیم گرفت.
            </h2>
          <ContactForm />
        </div>
      </div>
    </section>
  </Layout>
);

export default IndexPage;

export const query = graphql`
query {
  logo: file(relativePath: { eq: "icon-face.png" }) {
    childImageSharp {
      fixed(height: 40, width: 40) {
        ...GatsbyImageSharpFixed
      }
    }
  }
  map: file(relativePath: { eq: "map.png" }) {
    childImageSharp {
      fluid(maxHeight: 320, maxWidth: 480) {
        ...GatsbyImageSharpFluid
      }
    }
  }
  cover: file(relativePath: { eq: "cover.png" }) {
    childImageSharp {
      fixed (width: 2400) {
        src
      }
    }
  }
  services: allContentfulService ( sort:{ fields: priority, order: ASC } ) {
    edges {
      node {
        id
        slug
        title
        subtitle
        thumbnail {
          fixed (width: 96) {
            ...GatsbyContentfulFixed
          }
        }
      }
    }
  }
  steps: allContentfulStep ( sort:{ fields: order, order: ASC } ) {
    edges {
      node {
        id
        order
        name
        description
        picture {
          fixed (width: 64) {
            ...GatsbyContentfulFixed
          }
        }
      }
    }
  }
  seo: contentfulMetadata {
    name
    title
    author
    description {
      childMarkdownRemark {
        html
      }
    }
    keywords {
      childMarkdownRemark {
        html
      }
    }
  }
}
`;