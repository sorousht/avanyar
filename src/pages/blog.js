import React from "react";
import { Link, graphql } from "gatsby";
import Img from "gatsby-image";
import { Layout } from "../components"
import { css } from 'glamor';

const moment = require('moment-jalaali');
moment.loadPersian({
  usePersianDigits: true,
});

const Post = ({ data }) => {
  const main = css({
    margin: '36px 0 48px 0',
  });

  return (
    <Layout
      logo={data.logo}
      seo={data.seo}
      map={data.map}
      subtitle="وبلاگ"
    >
      <section className="container">
        <div {...main}>
          {
            data.posts.edges.map(({ node }) => (
              <Link to={`/blog/${node.slug}`} key={node.id}>
                <article className="card" style={{ marginBottom: 48 }}>
                  <div className="card-image">
                    <Img
                      style={{ height: 320 }}
                      fluid={node.cover.fluid}
                      objectFit="cover"
                      objectPosition="50% 50%"
                      alt={node.title}
                    />
                  </div>
                  <div className="card-content">
                    <h2 className="title is-size-5">
                      {node.title}
                    </h2>
                    <div className="media">
                      <div className="media-left">
                        <Img
                          fixed={node.author[0].avatar.fixed}
                          style={{ 'border-radius': '50%' }}
                        />
                      </div>
                      <div className="media-content">
                        <div className="content">
                          <strong>{node.author[0].name}</strong>
                          <p>
                            <small>{moment(node.createdAt).format("jD jMMMM jYYYY")}</small>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </article>
              </Link>
            ))
          }
        </div>
      </section>
    </Layout>
  );
}

export default Post;

export const pageQuery = graphql`
query {
  posts: allContentfulPost {
    edges {
      node {
        id
        createdAt
        title
        slug
        cover {
          fluid(maxHeight: 640, maxWidth: 1920) {
            srcSet
            base64
            sizes
            src
          }
        }
        author {
          name
          avatar {
            fixed(width: 48, height: 48) {
              base64
              src
              srcSet
              height
              width
            }
          }
        }
      }
    }
  }
  logo: file(relativePath: {eq: "icon-face.png" }) {
    childImageSharp {
      fixed(height: 40, width: 40) {
        ...GatsbyImageSharpFixed
      }
    }
  }
  seo: contentfulMetadata {
    name
    title
    author
    description {
      childMarkdownRemark {
        html
      }
    }
    keywords {
      childMarkdownRemark {
        html
      }
    }
  }
  map: file(relativePath: { eq: "map.png" }) {
    childImageSharp {
      fluid(maxHeight: 320, maxWidth: 480) {
        ...GatsbyImageSharpFluid
      }
    }
  }
}
`;