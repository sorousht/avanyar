import React from "react"
import { Layout } from "../components";
import { Link } from "gatsby";

const NotFoundPage = ({ data }) => (
  <Layout
    logo={data.logo}
    seo={data.seo}
    map={data.map}
    subtitle="404"
  >
    <section className="container is-fluid" style={{ margin: '36px 0 48px 0' }}>
      <h1 className="is-size-5">صفحه مورد نظر یافت نشد!</h1>
      <Link to="/">بازگشت به خانه</Link>
    </section>
  </Layout>
)

export default NotFoundPage;

export const query = graphql`
query {
  logo: file(relativePath: {eq: "icon-face.png" }) {
    childImageSharp {
      fixed(height: 40, width: 40) {
        ...GatsbyImageSharpFixed
      }
    }
  }
  seo: contentfulMetadata {
    name
    title
    author
    description {
      childMarkdownRemark {
        html
      }
    }
    keywords {
      childMarkdownRemark {
        html
      }
    }
  }
  map: file(relativePath: { eq: "map.png" }) {
    childImageSharp {
      fluid(maxHeight: 320, maxWidth: 480) {
        ...GatsbyImageSharpFluid
      }
    }
  }
}
`;