import React from "react";
import { graphql } from "gatsby";
import Img from "gatsby-image";
import { Layout } from "../../components"
import { css } from 'glamor';

const moment = require('moment-jalaali');
moment.loadPersian({
  usePersianDigits: true,
});

const title = css({
  margin: '8px 4px 0 4px',
});

const subtitle = css({
  margin: '4px',
});

const content = css({
  margin: '0 4px',
});

const post = css({
  marginBottom: '24px'
});

const main = css({
  margin: '36px 0 48px 0',
});
const Post = ({ data }) => (
  <Layout
    logo={data.logo}
    map={data.map}
    seo={data.seo}
    subtitle={data.post.title}
  >
    <section className="container">
      <div {...main}>
        <article {...post}>
          <h1
            className="is-size-3-desktop is-size-5-touch has-text-weight-bold"
            {...title}
          >
            {data.post.title}
          </h1>
          <p className="has-text-grey" {...subtitle}>{
            [
              'نوشته شده توسط',
              data.post.author[0].name,
              'در تاریخ',
              moment(data.post.createdAt).format("jD jMMMM jYYYY"),
            ].join(' ')
          }</p>
          <Img
            className="is-block image"
            fluid={data.post.cover.fluid}
            objectFit="cover"
            objectPosition="50% 50%"
            alt={data.post.title}
            style={{
              margin: '14px 0 18px 0',
              height: 320,
            }}
          />
          <p
            className="content is-size-5-desktop"
            dangerouslySetInnerHTML={{
              __html: data.post.content.childMarkdownRemark.html,
            }}
            {...content}
          />
        </article>
        <div className="tile is-parent">
          <div className="tile is-child box">
            <article className="media">
              <figure className="media-left">
                <Img
                  fixed={data.post.author[0].avatar.fixed}
                  style={{ 'border-radius': '50%' }}
                />
              </figure>
              <div className="media-content">
                <div className="content">
                  <p>
                    <strong style={{ marginLeft: 12 }}>
                      {data.post.author[0].name}
                    </strong>
                    <small>{moment(data.post.createdAt).format('jYYYY/jM/jD')}</small>
                  </p>
                  <p className="is-block">{data.post.author[0].bio}</p>
                </div>
              </div>
            </article>
          </div>
        </div>
      </div>
    </section>
  </Layout>
);

export default Post;

export const pageQuery = graphql`
query($slug: String!) {
  post: contentfulPost(slug: { eq: $slug }) {
    title
    slug
    content {
      childMarkdownRemark {
        html
      }
    }
    author {
      name
      bio
      avatar {
        fixed(width: 64, height: 64) {
          base64
          src
          srcSet
          height
          width
        }
      }
    }
    cover {
      fluid(maxHeight: 640) {
        srcSet
        base64
        sizes
        src
      }
    }
    createdAt
  }
  logo: file(relativePath: { eq: "icon-face.png" }) {
    childImageSharp {
      fixed(height: 40, width: 40) {
        ...GatsbyImageSharpFixed
      }
    }
  }
  seo: contentfulMetadata {
    name
    title
    author
    description {
      childMarkdownRemark {
        html
      }
    }
    keywords {
      childMarkdownRemark {
        html
      }
    }
  }
  map: file(relativePath: { eq: "map.png" }) {
    childImageSharp {
      fluid(maxHeight: 320, maxWidth: 480) {
        ...GatsbyImageSharpFluid
      }
    }
  }
}
`;