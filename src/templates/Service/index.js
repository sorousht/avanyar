import React from "react";
import { graphql } from "gatsby";
import Img from "gatsby-image";
import { Layout } from "../../components"
import { css } from 'glamor';
import "../../styles/main.scss";

const moment = require('moment-jalaali');
moment.loadPersian({
  usePersianDigits: true,
});

const title = css({
  marginBottom: '8px',
});

const subtitle = css({
  marginBottom: '4px',
});

const post = css({
  marginBottom: '24px'
});

const main = css({
  margin: '36px 0 48px 0',
});

const Service = ({ data }) => (
  <Layout
    logo={data.logo}
    map={data.map}
    seo={data.seo}
    subtitle={`خدمات ${data.service.title}`}
  >
    <section className="container">
      <div {...main}>
        <article {...post}>
          <h1
            className="is-size-3-desktop is-size-5-touch has-text-weight-bold"
            {...title}
          >
            {data.service.title}
          </h1>
          <h2
            className="is-size-5-desktop is-size-6-touch has-text-grey	"
            {...subtitle}
          >
            {data.service.subtitle}
          </h2>
          <Img
            className="is-block image"
            fluid={data.service.cover.fluid}
            objectFit="cover"
            objectPosition="50% 50%"
            alt={data.service.title}
            style={{
              margin: '14px 0 18px 0',
              height: 320,
            }}
          />
          <p
            className="content is-size-5-desktop"
            dangerouslySetInnerHTML={{
              __html: data.service.description.childMarkdownRemark.html,
            }}
          />
        </article>
      </div>
    </section>
  </Layout>
);

export default Service;

export const pageQuery = graphql`
query($slug: String!) {
  service: contentfulService(slug: { eq: $slug }) {
    title
    subtitle
    slug
    description {
      childMarkdownRemark {
        html
      }
    }
    cover {
      fluid(maxHeight: 640) {
        srcSet
        base64
        sizes
        src
      }
    }
  }
  logo: file(relativePath: { eq: "icon-face.png" }) {
    childImageSharp {
      fixed(height: 40, width: 40) {
        ...GatsbyImageSharpFixed
      }
    }
  }
  seo: contentfulMetadata {
    name
    title
    author
    description {
      childMarkdownRemark {
        html
      }
    }
    keywords {
      childMarkdownRemark {
        html
      }
    }
  }
  map: file(relativePath: { eq: "map.png" }) {
    childImageSharp {
      fluid(maxHeight: 320, maxWidth: 480) {
        ...GatsbyImageSharpFluid
      }
    }
  }
}
`;