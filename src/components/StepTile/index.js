import React from "react";
import Img from "gatsby-image";
import persianJs from "persianjs";

export const StepTile = ({ steps }) => {
  return (
    <div className="columns">
      {
        steps.edges.map((item) => (
          <div className="column" key={item.node.id}>
            <article>
              <Img
                fixed={item.node.picture.fixed}
                alt={item.node.title}
              />
              <p className="is-size-4 has-text-weight-bold">{persianJs(item.node.order).englishNumber().toString()}.&nbsp;{item.node.name}</p>
              <p className="">{item.node.description}</p>
            </article>
          </div>
        ))
      }
    </div>
  );
};