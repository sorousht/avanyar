import React from 'react';
import { useState } from 'react';
import { produce } from 'immer';
import classNames from 'classnames';

export const ContactForm = () => {

  const [name, setName] = useState('');
  const [contact, setContact] = useState('');
  const [body, setBody] = useState('');
  const [submitted, setSubmitted] = useState(false);

  const [errors, setErrors] = useState({
    name: false,
    contact: false,
    body: false,
  });

  const handleNameChange = (event) => {
    setName(event.target.value);
    setErrors(produce(errors, (draft) => {
      draft.name = !event.target.value;
    }));
  };

  const handleContactChange = (event) => {
    setContact(event.target.value);
    setErrors(produce(errors, (draft) => {
      draft.contact = !event.target.value;
    }));
  };

  const handleMessageChange = (event) => {
    setBody(event.target.value);
    setErrors(produce(errors, (draft) => {
      draft.body = !event.target.value;
    }));
  };

  const validate = () => {
    return {
      name: !name,
      contact: !contact,
      body: !body,
    };
  };

  const handleSubmit = (event) => {
    const errors = validate();

    setSubmitted(true);
    setErrors(errors);

    if (!formIsValid(errors)) {
      event.preventDefault();
      return;
    }
  };

  const disabled = submitted && !formIsValid(errors);

  return (
    <form
      name="contact"
      method="POST"
      action="/thanks"
      data-netlify="true"
      onSubmit={handleSubmit}
      netlify-honeypot="bot-field"
      style={{ maxWidth: 512, margin: '0 auto' }}
    >
      <div className="field">
        <label className="label" for="name_input">نام</label>
        <div className="control">
          <input
            id="name_input"
            name="name"
            className={classNames("input", getFieldClass(submitted, errors.name))}
            type="text"
            placeholder="نام"
            value={name}
            onChange={handleNameChange}
          />
        </div>
        {submitted && errors.name ? <p className="help is-danger">لطفا نام و نام‌خانوادگی خود را بنویسید</p> : null}
      </div>
      <div className="field">
        <label className="label" for="email_phone_input">تماس</label>
        <div className="control">
          <input
            id="email_phone_input"
            className={classNames("input", getFieldClass(submitted, errors.contact))}
            type="text"
            placeholder="شماره تماس یا آدرس ایمیل"
            name="contact"
            value={contact}
            onChange={handleContactChange}
          />
        </div>
        {submitted && errors.contact ? <p className="help is-danger">لطفا شماره تماس یا آدرس ایمیل خود را بنویسید</p> : null}
      </div>
      <div className="field">
        <label className="label" for="message_input">پیام</label>
        <div className="control">
          <textarea
            id="message_input"
            className={classNames("textarea", getFieldClass(submitted, errors.body))}
            placeholder="متن"
            rows={8}
            name="body"
            value={body}
            onChange={handleMessageChange}
          />
        </div>
        {submitted && errors.body ? <p className="help is-danger">لطفا متن درخواست خود را بنویسید</p> : null}
      </div>
      <div className="field">
        <div className="control">
          <button
            type="submit"
            className="button is-primary is-medium is-outlined"
            disabled={disabled}
          >
            ارسال
          </button>
        </div>
      </div>
      <input type="hidden" name="form-name" value="contact" />
    </form>
  );
};

const formIsValid = (errors) => {
  return Object.values(errors).every(r => !r);
};

const getFieldClass = (submitted, hasError) => {
  if (submitted) {
    return hasError ? 'is-danger' : 'is-success';
  }
  return '';
}