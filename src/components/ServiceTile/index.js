import React from "react";
import Img from "gatsby-image";
import { Link } from "gatsby";

export const ServiceTile = ({ services }) => {
  return (
    <div className="tile is-ancestor">
      {
        services.edges.map((item) => (
          <div className="tile is-parent" key={item.node.id}>
            <Link to={`/service/${item.node.slug}`} title={item.node.subtitle} style={{ display: 'contents' }}>
              <article className="tile is-child notification is-warning">
                <p className="title is-size-4">{item.node.title}</p>
                <p className="subtitle is-size-6">{item.node.subtitle}</p>
                <Img
                  fixed={item.node.thumbnail.fixed}
                  alt={item.node.title}
                />
              </article>
            </Link>
          </div>
        ))
      }
    </div>
  );
};