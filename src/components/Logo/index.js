import React from 'react';
import Img from 'gatsby-image';
import classNames from "classnames";

export const Logo = ({ image, metadata, reverse }) => (
  <div className="level is-mobile" style={{ margin: '6px 0', maxWidth: 136 }}>
    <div className="level-item">
      <Img
        fixed={image.childImageSharp.fixed}
        title={metadata.name}
        alt={metadata.name}
      />
    </div>
    <div className="level-item">
      <h1 className={classNames("title", {
        'has-text-dark': !reverse,
        'has-text-white': reverse,
      })}>
        {metadata.name}
      </h1>
    </div>
  </div>
);