import React from 'react';
import { Footer, SEO, Navbar } from "../../components"
import { Helmet } from "react-helmet";
import "../../styles/main.scss";

export const Layout = ({
  seo,
  logo,
  map,
  children,
  hasNavbar = true,
  subtitle = "" }) => (
    <>
      <Helmet
        htmlAttributes={{
          dir: 'rtl',
          class: 'has-navbar-fixed-top',
        }}
      >
        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
      </Helmet>
      <SEO
        metadata={seo}
        subtitle={subtitle}
      />
      {hasNavbar && <Navbar
        seo={seo}
        logo={logo}
      />}
      {children}
      <Footer
        metadata={seo}
        logo={logo}
        map={map}
      />
    </>
  );
