import React from "react";
import { Helmet } from "react-helmet";

const createTitle = (name, subtitle) => `${name} | ${subtitle}`;
const stripHtmlTags = markup => markup.replace(/(<([^>]+)>)/ig, "");
const convertMultilineToSeperated = text => text.replace(/\r?\n|\r/g, ",");


export const SEO = ({ metadata, subtitle }) => {

  const {
    name,
    title,
    author,
    description,
    keywords,
  } = metadata;

  const safeTitle = subtitle ? createTitle(name, subtitle) : title;
  const safeDescription = stripHtmlTags(description.childMarkdownRemark.html);
  const safeKeywords = convertMultilineToSeperated(stripHtmlTags(keywords.childMarkdownRemark.html));

  return (
    <Helmet
      htmlAttributes={{
        lang: 'fa',
      }}
      title={safeTitle}
      meta={[
        {
          name: `description`,
          content: safeDescription,
        },
        {
          property: `og:title`,
          content: safeTitle,
        },
        {
          property: `og:description`,
          content: safeDescription,
        },
        {
          property: `og:type`,
          content: `website`,
        },
        {
          name: `twitter:card`,
          content: `summary`,
        },
        {
          name: `twitter:creator`,
          content: author,
        },
        {
          name: `twitter:title`,
          content: safeTitle,
        },
        {
          name: `twitter:description`,
          content: safeDescription,
        },
        {
          name: `keywords`,
          content: safeKeywords,
        }
      ]}
    />
  );
};
