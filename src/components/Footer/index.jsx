import React from "react";
import Img from 'gatsby-image';
import './style.scss';

export const Footer = ({ metadata, logo, map }) => (
  <footer className="footer">
    <div className="columns">
      <div className="column brand">
        <div className="logo is-mobile">
          <Img
            fixed={logo.childImageSharp.fixed}
            title={metadata.name}
            alt={metadata.name}
            style={{ marginLeft: 6 }}
          />
          <span className="is-size-3 has-text-weight-bold	">
            {metadata.name}
          </span>
        </div>
        <p className="description" dangerouslySetInnerHTML={{ __html: metadata.description.childMarkdownRemark.html }} />
        <p>
          <br />
          <span className="is-size-5 has-text-weight-medium">۰۷۱-۹۱۰۰۲۲۰۳</span>
          <br />
          <span className="is-size-5 has-text-weight-medium">۰۹۹۰-۳۳۳۷۵۰۵</span>
          <br />
          <a href="mailto:hi@avanyar.ir">info@avanyar.ir</a>
        </p>
      </div>
    </div>
    <div className="level">
      <div className="level-item">
        <p className="has-text-centered">
          تمامی حقوق مادی و معنوی به <a href="/"
            title="آوان‌یار" > آوان‌یار </a> تعلق دارد.
          </p>
      </div>
    </div>
  </footer >
);