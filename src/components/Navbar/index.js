import React from 'react';
import { useState } from "react";
import classNames from "classnames";
import { Link } from "gatsby";
import { Logo } from "../";

export const Navbar = ({ seo, logo, isPrimary = false }) => {
  const [burgerIsActive, setBurgerIsActive] = useState(false);
  const closeMenu = () => setBurgerIsActive(false);
  return (
    <nav className={classNames("navbar is-fixed-top", { "is-primary": isPrimary, "has-background-primary": isPrimary })}>
      <div className="container is-fluid">
        <div className="navbar-brand" role="navigation" aria-label="main navigation">
          <Link
            to="/"
          >
            <Logo
              metadata={seo}
              image={logo}
              reverse={isPrimary}
            />
          </Link>
          <a
            href="#"
            role="button"
            className={classNames("navbar-burger burger", burgerIsActive ? "is-active" : null)}
            aria-label="menu"
            aria-expanded="false"
            data-target="navbarMenu"
            onClick={() => setBurgerIsActive(!burgerIsActive)}
          >
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </a>
        </div>
        <div id="navbarMenu" className={classNames("navbar-menu", { "is-active": burgerIsActive })}>
          <div className="navbar-start">
            <Link
              className="navbar-item has-text-weight-bold"
              onClick={closeMenu}
              to="/#services"
            >
              خدمات
          </Link>
            <Link
              className="navbar-item has-text-weight-bold"
              to="/#workflow"
              onClick={closeMenu}
            >
              روش‌کار
          </Link>
            <Link
              className="navbar-item has-text-weight-bold"
              to="/#contact"
              onClick={closeMenu}
            >
              تماس
          </Link>
            <Link
              className="navbar-item has-text-weight-bold"
              to="/blog"
            >
              بلاگ
          </Link>
          </div>
        </div>
      </div>
    </nav >
  );
};
